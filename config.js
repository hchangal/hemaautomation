import * as path from "path";
import { browser, Config } from "protractor";
import { Reporter } from "../support/reporter";
const jsonReports = process.cwd() + "/reports/json";

export const config: Config = {

    seleniumAddress: "http://10.200.200.1:4444/wd/hub",

    SELENIUM_PROMISE_MANAGER: false,

    directConnect: true,

    baseUrl: "https://www.dev.security-connect.com/",

    capabilities: {
        browserName: 'firefox',
        'goog:chromeOptions': {
            'w3c': false
        }
    },

    framework: "custom",
    frameworkPath: require.resolve("protractor-cucumber-framework"),

    specs: [
        "../../features/*.feature",
    ],

    onPrepare: () => {
        browser.ignoreSynchronization = true;
        browser.manage().window().maximize();
        Reporter.createDirectory(jsonReports);
    },

    cucumberOpts: {
        compiler: "ts:ts-node/register",
        format: "json:./reports/json/cucumber_report.json",
        require: ["../../typeScript/stepdefinitions/*.js", "../../typeScript/support/*.js"],
        strict: true,
        dry: false,
        tags: "@regression",
    },

    onComplete: () => {
        Reporter.createHTMLReport();
    },
};
