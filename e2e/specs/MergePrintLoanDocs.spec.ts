import { browser, $ } from 'protractor';

import { Login } from '../pages/login.po'

import { constants } from 'zlib';
import { MergePrintLoanDocs } from '../pages/mergePrintLoandocs.po';
const { setDefaultTimeout, Given, When, Then } = require("cucumber");

const login: Login = new Login()
const mergePrintLoanDocs: MergePrintLoanDocs = new MergePrintLoanDocs



// spec.js
Given('I am on application homepage', async function () {
    
        await login.goto()
})

When('I press login button and go to mergePrintLoanDocs', async function () {
        await login.loginAs('narefin', 'P@ssword1!')

        mergePrintLoanDocs.goto()

        await mergePrintLoanDocs.mergePrintLoanDocs('TT8010106DC (Test Client)', 'SLS - Private - LR Post', '2')

        await browser.sleep(5000)

        // <option value="TT8010106DC">TT8010106DC (Test Client)</option>


    })

    Then('file downloads after pressing mergegenerate button', async function () {
       
    }); 
