
import { browser, $ } from 'protractor';

import { Login } from '../pages/login.po'
import { Report } from '../pages/reportPage.po'
import { Filestatuschange } from '../pages/filestatuschange.po'
import { UpdateTracking } from '../pages/updateTracking.po'
import {UpdateDates} from '../pages/updateDates.po'
import {SearchUploadImages} from '../pages/imageupload.po'
import { constants } from 'zlib';

const login: Login = new Login()
const report: Report = new Report()
const filestatuschange: Filestatuschange = new Filestatuschange()
const updateTracking: UpdateTracking = new UpdateTracking()
const updateDates: UpdateDates = new UpdateDates()
const imageUpload: SearchUploadImages = new SearchUploadImages


// spec.js
describe('Protractor Demo App', function () {
  it('should have a title', async function () {
    // await browser.get('sci.framework.web/Portal/External/Login.aspx');

    // 
    // await $('input[name="ctl00$ctl00$baseBody$body$username"]').sendKeys('narefin')
    // await $('input[name="ctl00$ctl00$baseBody$body$password"]').sendKeys('P@ssword1!')
    // await browser.sleep(10000)
    // await $('input[name="ctl00$ctl00$baseBody$body$btnLogin"]').click()
    await login.goto()
    await login.loginAs('narefin', 'P@ssword1!')

    // await browser.sleep(5000)

    // await browser.get('SCI.Framework.Web/Reports/ReportList.aspx')

    // await browser.sleep(5000)

    await updateTracking.goto()

    await browser.sleep(3000)
    await updateTracking.selectProject('TT8010106DC', 'Assignment of Mortgage' , 'Date Rescission Sent to County')
    // await updateTracking.selectProject('10')

    // await browser.sleep(60000)
    await updateDates.goto()

    await browser.sleep(3000)
    await updateDates.selectProjectUd('TT8010106DC', 'Assignment of Mortgage' , 'Date Rescission sent to QA')

    await browser.sleep(3000)
    await imageUpload.goto()
    await imageUpload.imageSearch('TT8010106DC', '2')
    await browser.sleep(30000000)

    




  

    await report.goto()

    await report.findReport('File Status Rules - Change [Management][Workflow]')

    await browser.sleep(3000)

    var results = await filestatuschange.determineFilestatus('Accommodation Assignment of Mortgage 2')

    results.forEach(function (item) {
      item.getText().then(console.log)
    })

    await browser.sleep(10000)

    //  await home.searchFor('selenium')

    await browser.sleep(5000)


  });
});