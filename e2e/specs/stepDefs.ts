import { browser, $, By,element, ExpectedConditions } from 'protractor';

import { Login } from '../pages/login.po'
import { TableDefinition } from "cucumber";
import { MergePrintLoanDocs } from '../pages/mergePrintLoandocs.po';
import { JdtDataEntry } from '../pages/jdtDataEntry.po';
import { DataReview } from '../pages/dataReview.po'
import { AsmtCountyOverDue } from '../pages/asmtCountyOverdue.po'
import { SearchUploadImages } from '../pages/imageupload.po'
import { UploadImages } from '../pages/imageUpload1.po';
import { SearchUploadImagesCleanup } from '../pages/imageuploadCleanUp.po'
import { ReportPage } from '../pages/reportPage.po'
import { FileStatusRulesPage } from '../pages/fileStatusRulesPage'
import { SciStaffPage } from '../pages/sciStaffPage'
import { HomePage } from '../pages/homePage'
import { StateCounty } from '../pages/statesCounties.po'
import { CreateNewUser } from '../pages/NewUserCreate.po';
import { DocumentHistory } from '../pages/documentHistory.po'
import { DocumentCheckIn } from '../pages/documentCheckIn.po';
import { ImageSearch } from '../pages/imageSearch.po'

const { setDefaultTimeout, Given, When, Then } = require("cucumber");
let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;

const login: Login = new Login()
const mergePrintLoanDocs: MergePrintLoanDocs = new MergePrintLoanDocs
const jdtDataEntry: JdtDataEntry = new JdtDataEntry
const dataReview: DataReview = new DataReview
const asmtCountyOverdue: AsmtCountyOverDue = new AsmtCountyOverDue
const imageUpload: SearchUploadImages = new SearchUploadImages
const imageUploadCleanup: SearchUploadImagesCleanup = new SearchUploadImagesCleanup
const imageUpload1: UploadImages = new UploadImages
const reports: ReportPage = new ReportPage();
const filestatusRule: FileStatusRulesPage = new FileStatusRulesPage();
const scistaff: SciStaffPage = new SciStaffPage();
const home: HomePage = new HomePage();
const stateCounty: StateCounty = new StateCounty()
const createNewUser: CreateNewUser = new CreateNewUser()
const documentHistory: DocumentHistory = new DocumentHistory()
const documentCheckIn: DocumentCheckIn = new DocumentCheckIn()
const imageSearch: ImageSearch = new ImageSearch()
const username: string = browser.params.credentials.username
const password: string = browser.params.credentials.password


setDefaultTimeout(60000);

Given('User logs into application', async function () {
  await login.goto()
  await login.loginAs(username, password)
});



When('User selects appropriate {string} and {string}', async function (project: string, mergeProcess: string) {
  mergePrintLoanDocs.goto()

  await mergePrintLoanDocs.mergePrintLoanDocs('TT8010106DC (Test Client)', 'SLS - Private - LR Post', '2')

  await browser.sleep(5000)
});


When('Enter loan number and click mergeBtn', async function () {
  // Write code here that turns the phrase above into concrete actions
  // return 'pending';
  await browser.sleep(3000)
});


Then('pdf file gets downloaded', function () {
  // Write code here that turns the phrase above into concrete actions

});


When('User selects appropriate {string} and {string} and {string}', async function (string, string2, string3) {
 // Write code here that turns the phrase above into concrete actions
mergePrintLoanDocs.goto()

await mergePrintLoanDocs.mergePrintLoanDocs('TT8010106DC (Test Client)', 'SLS - Private - LR Post', '2')

await browser.sleep(5000)
//  return 'pending';
});



Then('pdf file doesnt get downloaded', function () {
// Write code here that turns the phrase above into concrete actions
return expect(mergePrintLoanDocs.getErrorMsg().getText()).to.eventually.contains('Merge was unsuccessful.')
           
//  return 'pending';
});

Given('User logs into application to test jdtdata entry', async function () {
  login.goto()
  await login.loginAs(username, password)
});


When('User selects appropriate {string} and {string} and {string} and {string}', async function (project: string, reference: string, jdt: string, legaltxt: string) {
  jdtDataEntry.goto()

  await jdtDataEntry.jdtDataEntry('TT8010106DC (TT8010106DC (Test Client))', '1', 'Assignment of Mortgage', 'testest')

  await browser.sleep(5000)
});



When('Enter reference number and input data in legal field', function () {
  // Write code here that turns the phrase above into concrete actions

});

When('Submit form', function () {
  // Write code here that turns the phrase above into concrete actions

});

Then('jdt data gets updated', function () {
  // Write code here that turns the phrase above into concrete actions

});

Given('User logs into application to test datareview', async function () {
  await login.goto()
  await login.loginAs(username, password)
});


// When User selects appropriate "project" and "document" and "reference"
// Undefined. Implement with the following snippet:

When('User on DataReview Note page selects appropriate {string} and {string} and {string} and {string}', async function (project: string, document: string, refrence: string, address: string) {
  dataReview.goto()

  await dataReview.dataReviewChangeAddress(project, document, refrence, address)

  await browser.sleep(5000)

});

// ? And Make Few changes
// Undefined. Implement with the following snippet:

When('Make Few changes on note', function () {
  // selecting radio button on lookback

});

// ? And Save changes
// Undefined. Implement with the following snippet:

When('Save changes on note', function () {
  // save changes

});

// ? Then data review gets updated
// Undefined. Implement with the following snippet:

Then('data review gets updated on note', function () {
  // Write code here that turns the phrase above into concrete actions

});

When('User on DataReview ReferenceData page selects appropriate {string} and {string} and {string} and {string}', async function (project: string, document: string, refrence: string, legalDes: string) {
  dataReview.goto()

  await dataReview.dataReviewChangeLegalDescription(project, document, refrence, legalDes)

  await browser.sleep(5000)

});

// ? And Make Few changes
// Undefined. Implement with the following snippet:

When('Make Few changes on referencedata', function () {
  // selecting radio button on lookback

});

// ? And Save changes
// Undefined. Implement with the following snippet:

When('Save changes on referencedata', function () {
  // save changes

});

// ? Then data review gets updated
// Undefined. Implement with the following snippet:

Then('data review gets updated on referencedata', async function () {
  // Write code here that turns the phrase above into concrete actions
  return expect(dataReview.getDataSavedText().getText()).to.eventually.equal('Data saved')

});

// When User goes to reports for html
//      Undefined. Implement with the following snippet:

When('User goes to reports for html', async function () {
  // Write code here that turns the phrase above into concrete actions
  await reports.open();
  await reports.on();
  await reports.pickReport('ASMT - County Overdue [Mortgage Services][Assignment JDTs]');
  await filestatusRule.on();
  // filestatusRule.jdtDropDown.all(By.tagName('option')).each(function(el){
  //     el.getText().then(console.log);
  // });
  // await filestatusRule.selectJdt('Accommodation Lien Release');
  // filestatusRule.fileStatusDropDown.all(By.tagName('option')).each(function (el) {
  //     el.getText().then(console.log);
  // });
});

When('User on asmt county page for html report and selects appropriate {string} and {string}', async function (project, jdt) {
  // Write code here that turns the phrase above into concrete actions

  await asmtCountyOverdue.asmtCountyOverduehtml(project, jdt)

  await browser.sleep(5000)

});



When('click generate button for html report', function () {
  // Write code here that turns the phrase above into concrete actions

});



Then('html report displays', function () {
  // Write code here that turns the phrase above into concrete actions

});
// When User goes to reports for excel
// Undefined. Implement with the following snippet:

When('User goes to reports for excel', async function () {
  // Write code here that turns the phrase above into concrete actions
  await reports.open();
  await reports.on();
  await reports.pickReport('ASMT - County Overdue [Mortgage Services][Assignment JDTs]');
  await filestatusRule.on();

  // return 'pending';
});


When('User on asmt county page for excel report and selects appropriate {string} and {string}', async function (project, jdt) {
  // Write code here that turns the phrase above into concrete actions
  // asmtCountyOverdue.goto()

  await asmtCountyOverdue.asmtCountyOverdueExcel(project, jdt)

  await browser.sleep(5000)

});



When('click generate button for excel report', function () {
  // Write code here that turns the phrase above into concrete actions

});



Then('excel report displays', function () {
  // Write code here that turns the phrase above into concrete actions

});

// When User goes to reports for csv
//    Undefined. Implement with the following snippet:

When('User goes to reports for csv', async function () {
  // Write code here that turns the phrase above into concrete actions
  await reports.open();
  await reports.on();
  await reports.pickReport('ASMT - County Overdue [Mortgage Services][Assignment JDTs]');
  await filestatusRule.on();
  //  return 'pending';
});

When('User on asmt county page for csv report and selects appropriate {string} and {string}', async function (project, jdt) {
  // Write code here that turns the phrase above into concrete actions
  // asmtCountyOverdue.goto()

  await asmtCountyOverdue.asmtCountyOverdueCsv('TT8010106DC', 'Assignment of Mortgage')

  await browser.sleep(5000)

});



When('click generate button for csv report', function () {
  // Write code here that turns the phrase above into concrete actions

});



Then('csv report displays', function () {
  // Write code here that turns the phrase above into concrete actions

});

// When on image search & upload page User selects appropriate "project" and "accountNumber"
//    Undefined. Implement with the following snippet:


When('on image search & upload page User selects appropriate {string} and {string}', async function (project, accountNumber) {
  // Write code here that turns the phrase above into concrete actions
  await imageUpload.goto()

  await imageUpload.imageSearch(project, accountNumber)
  await browser.sleep(5000)

  //  return 'pending';
});



//Cleanup
When('on image search & upload page User selects appropriate {string} and {string} for cleanup', async function (project, accountNumber) {
  // Write code here that turns the phrase above into concrete actions
  await imageUploadCleanup.goto()

  await imageUploadCleanup.imageSearchCleanup(project, accountNumber)
  
  await browser.sleep(2000)


  //  return 'pending';
});




When('Search form', function () {
  // Write code here that turns the phrase above into concrete actions
  //  return 'pending';
});


When('goes to the loan', function () {
  // Write code here that turns the phrase above into concrete actions
  //  return 'pending';
});


When('selects image submission category', async function () {
  // Write code here that turns the phrase above into concrete actions
  await imageUpload1.imageSearch1('AOM (Assignment of Mortgage)')
  await browser.sleep(5000)

  //  return 'pending';
});

When('selects image submission category for existing image test', async function () {
  // Write code here that turns the phrase above into concrete actions
  await imageUpload1.imageSearch1('1003-FINAL (1003 - Final)')
  await browser.sleep(5000)
  // return 'pending';
});

When('selects an image and upload', function () {
  // Write code here that turns the phrase above into concrete actions
  //  return 'pending';
});

Then('iamge gets uploaded', async function () {
  // Write code here that turns the phrase above into concrete actions
  // var x = await imageUpload1.getDialogWindow().getText()
  // console.log('X is:', x)
  return expect(imageUpload1.getDialogWindow().getText()).to.eventually.equal('Uploaded')
  //  expect(imageUpload1.getDialogWindow().getText()).to.eventually.have.text('Uploaded') 
  //  or expect(el).to.contain.text('some text')
  // expect(x).to.contain('Warn')
  //  return 'pending';
});
Then('iamge gets warning', async function () {
  // Write code here that turns the phrase above into concrete actions
  return expect(imageUpload1.getDialogWindow().getText()).to.eventually.have.equal('Warning')
});

When('User on Image Search page selects appropriate {string} and {string}', async function (string, string2) {
  // Write code here that turns the phrase above into concrete actions
  imageSearch.goto()
  await imageSearch.imageSearch(string, string2)
  //await imageSearch.imageSearch('FinProd108', 'TT8010106DC (Test Client)')
});



When('search for images', function () {
  // Write code here that turns the phrase above into concrete actions

});



When('user on image search page selects appropriate {string} to view the JDT', async function (string) {
  // Write code here that turns the phrase above into concrete actions
  await imageSearch.imageSearchCheck('Assignment of Mortgage')

  return expect(imageSearch.getTableImageSearch().getText()).to.eventually.contains('Assignment of Mortgage')

});



Then('uploaded image displays under JDT', function () {
  // Write code here that turns the phrase above into concrete actions

});


         When('User on Image Search page selects appropriate {string} and {string} to test existing image', async function (string, string2) {
           // Write code here that turns the phrase above into concrete actions
           imageSearch.goto()

           await imageSearch.imageSearch('FinProd108', 'TT8010106DC (Test Client)')
         });

 

         When('user on image search page selects appropriate {string} to view the JDT to test existing image', async function (string) {
           // Write code here that turns the phrase above into concrete actions
           await imageSearch.imageSearchCheck('1003 - Final')

           return expect(imageSearch.getTableImageSearch().getText()).to.eventually.contains('1003 - Final')
         });


         Then('Existing image displays under JDT', function () {
           // Write code here that turns the phrase above into concrete actions
          //  return 'pending';
         });

         When('user on image search page selects appropriate {string} to view the JDT to test existing tif image', async function (string) {
          //  Write code here that turns the phrase above into concrete actions
           await imageSearch.imageSearchCheck('TitlePolicy')
          return expect(imageSearch.getTableImageSearch().getText()).to.eventually.contains('TitlePolicy')
                    
                    
           });
          //  When('user on image search page selects appropriate {string} to view the JDT to test existing tif image', async function (string) {
          //   // Write code here that turns the phrase above into concrete actions
          //   return expect(imageSearch.getTableImageSearch().getText()).to.eventually.contains('TitlePolicy')//   // return 'pending';
                      // });
                
          When('user on image search page selects loan check box to merge and click merge', async function () {
           // Write code here that turns the phrase above into concrete actions
          await imageSearch.imageMerge()
                        
             // return 'pending';
          });
                      
          Then('Existing images gets merged',async function () {
                        
          });
                    
          When('user on image search page selects loan check box to merge and click merge tif', async function () {
                        // Write code here that turns the phrase above into concrete actions
          await imageSearch.imageMergeTif()
                       
          });
                    
          When('user on image search page selects loan check box to merge and download csv', async function () {
                        // Write code here that turns the phrase above into concrete actions
          await imageSearch.imageCsvDownload()
          await browser.sleep(5000)
          // return 'pending';
          });

When('User selects appropriate Reports from menu', async function () {
  await reports.open();
  await reports.on();
  await reports.pickReport('File Status Rules - Change [Management][Workflow]');
  await filestatusRule.on();
  // filestatusRule.jdtDropDown.all(By.tagName('option')).each(function(el){
  //     el.getText().then(console.log);
  // });
  await filestatusRule.selectJdt('Accommodation Lien Release');
  // filestatusRule.fileStatusDropDown.all(By.tagName('option')).each(function (el) {
  //     el.getText().then(console.log);
  // });
});

Then('new statuses exist in File status', async function (dataTable: TableDefinition) {
  var rows = dataTable.rows();

  async function processJdtDropdown(f) {
    var jdtItems = await filestatusRule.jdtDropDown.all(By.tagName('option'));
    for (const jdtItem of jdtItems) {
      await jdtItem.click();
      await f();
    }
  }

  async function processFileStatus() {
    for (const fileItem of rows) {
      await filestatusRule.fileStatusDropDown.element(By.xpath("//option[text()='" + fileItem[0] + "']")).click();
    }
  }

  await filestatusRule.on()
  await processJdtDropdown(processFileStatus);
  await browser.sleep(500);

});

Then('wait {int}', async function (sleep) {
  await browser.sleep(sleep);
});

When('User selects appropriate {string} and final {string}', async function (menus, menu) {
  var menusArray = menus.replace(/, /g, ",").split(",");
  await home.on();
  await home.menu(...menusArray, menu);
});

Then('User ends up on final {string} page', (menu) => {
  return expect(home.pageHeaderTitle.getText()).to.eventually.equal(menu);
});

When('User hovers mouse over picture on SCI Staff page', async function () {
  var element = scistaff.images.first();
  await browser.executeScript("if(document.createEvent){" +
    "var hoverEventObj = document.createEvent('MouseEvents');" +
    "hoverEventObj.initEvent('mouseover',true,false);" +
    "arguments[0].dispatchEvent(hoverEventObj);" +
    "}" +
    "else if(document.createEventObject){" +
    "arguments[0].fireEvent('onmouseover');" +
    "}", element);
});

Then('selected image enlarges', async function () {
  return expect(scistaff.images.first().getAttribute("style")).to.eventually.contain("scale(2)");
});

// When User on state and county page selects appropriate "type" and "parent" and "county" and "jdt"
// Undefined. Implement with the following snippet:

When('User on state and county page selects appropriate {string} and {string} and {string}', async function (state, county, jdt) {
  // Write code here that turns the phrase above into concrete actions
  stateCounty.goto()

  await stateCounty.stateCounty('Minnesota', 'Aitkin (T)', 'Assignment of Mortgage')


  // return 'pending';
});



When('User on state and couty page click JDT data entry requirments', function () {
  // Write code here that turns the phrase above into concrete actions

});



When('user on state and county page selects a jdt type', function () {
  // Write code here that turns the phrase above into concrete actions

});



When('user on state and county page enters data entry requirments and saves', function () {
  // Write code here that turns the phrase above into concrete actions

});



Then('user on state and county page is able to save correct format', function () {
  // Write code here that turns the phrase above into concrete actions

});





When('user on document checkin page User selects appropriate {string} and {string} and {string} and {string} and {string} and {string} and {string} and {string}', async function (string, string2, string3, string4, string5, string6, string7, string8) {
  // Write code here that turns the phrase above into concrete actions
  documentCheckIn.goto()

  await documentCheckIn.documentCheckIn(string, string2, string3, string4, string5, string6, string7, string8)
});



Then('user on document checkin page submit the loan', function () {
  // Write code here that turns the phrase above into concrete actions

});


Then('user on document checkin page generate document', function () {
  // Write code here that turns the phrase above into concrete actions
  // return $(this.pdfToolbar)
  
  // return expect(pdfValidation.getPdfToolbar().getText()).to.eventually.contains('A')
  

});



When('user on document history User selects appropriate {string} and {string} and {string} and {string} and {string}', async function (project, reference, fileType, docType, classType) {
  // Write code here that turns the phrase above into concrete actionscreateNewUser.goto()
  documentHistory.goto()

  await documentHistory.documentHistory('TT8010106DC (Test Client)', 'HCTest001', 'No File Type', 'Assignment of Mortgage', 'ORIGINAL')


});



Then('user can view document history for that loan', async function () {
  // Write code here that turns the phrase above into concrete actions

  return expect(documentHistory.getHistoryBox().getText()).to.eventually.contains('received')

});

//        Given User logs into application # e2e\specs\stepDefs.ts:79
//  ? And select Create New User button
//      Undefined. Implement with the following snippet:

Given('select Create New User button', function () {
  // Write code here that turns the phrase above into concrete actions
  //  return 'pending';
});



When('user on CreateNewUser page User enters appropriate', async function (values) {
  createNewUser.goto()
  console.log("DOCSTRING is:", values)
  var json = JSON.parse(values)
  await createNewUser.createNewUser(json.username, json.firstname, json.lastname, json.password, json.email)
  await browser.sleep(5000)
});



When('password is one character not in username', function () {
  // Write code here that turns the phrase above into concrete actions
  //  return 'pending';
});


Then('error popup occurs', function () {
  // Write code here that turns the phrase above into concrete actions
  //  return 'pending';
});

Then('error message {string} is present', async function (value: string) {
  var result: string = ""
  try {
    result = await browser.element(By.xpath("//*[text()='" + value + "']")).getText()
  } catch (e) {
    //noop
  }
  expect(result).to.equal(value)
});




Then('user is not able to login with new credentials', function () {
  // Write code here that turns the phrase above into concrete actions
  //  return 'pending';
})


When('password is string only password not in username', function () {
  // Write code here that turns the phrase above into concrete actions
  
});

When('password is one character and one number', function () {
  // Write code here that turns the phrase above into concrete actions
  
});


When('password is following all the requirments', function () {
  // Write code here that turns the phrase above into concrete actions
  // return 'pending';
});