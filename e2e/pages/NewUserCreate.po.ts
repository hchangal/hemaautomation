import { $,$$, browser, ElementFinder, element, By, ExpectedConditions, protractor, ElementArrayFinder } from "protractor"
import { Selector } from '../support/selector'



export class CreateNewUser {

    createNewUserBtn: string = '#ctl00_ctl00_baseBody_body_btnCreate'
                                //  ctl00_ctl00_baseBody_body_btnCreate
    userNameCreate: string = '#txtUsername';
    firstNameCreate: string = '#txtFirstName'
    lastNameCreate: string = '#txtLastName'
    passwordCreate: string = '#txtPassword'
    emailCreate: string = '#txtEmail'
    saveBtnCreateNewUser: string = '#ctl00_ctl00_baseBody_body_btnSave'
    modalClosebtnCreateUser: string = '.modal-footer > button'
    errorMsg: string = '#ctl00_ctl00_baseBody_body_ulPasswordValidationErrors'


    constructor() {
    }
    goto() {

        browser.get('sci/administration_usermanagement.aspx')
        // browser.sleep(3000)
  }

    getCreateNewUserBtn(){
        return $(this.createNewUserBtn)
    }
    getUserNameCreate() {

        return $(this.userNameCreate)
    }

    getFirstNameCreate() {
        return $(this.firstNameCreate)
    }

    getLastNameCreate() {
        return $(this.lastNameCreate)
    }

    getPasswordCreate() {
        return $(this.passwordCreate)
    }

    getSaveBtnCreateNewUser() {
        return $(this.saveBtnCreateNewUser)
    }

    getEmailCreate() {
        return $(this.emailCreate)
    }
    
    getModalCloseBtnCreateUser() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.modalClosebtnCreateUser)));
        return $(this.modalClosebtnCreateUser)
    }

    getErrorMsg() {
        return $(this.errorMsg)
    }

    async createNewUser(username: string, firstName: string, lastName: string, password: string, email:string) {

        try {

        await this.getCreateNewUserBtn().click()

        await browser.sleep(3000)

        await this.getUserNameCreate().sendKeys(username)
        await this.getFirstNameCreate().sendKeys(firstName)
        await this.getLastNameCreate().sendKeys(lastName)
        await this.getPasswordCreate().sendKeys(password)
        await this.getEmailCreate().sendKeys(email)

        await this.getSaveBtnCreateNewUser().click()

        await browser.sleep(2000)

        this.getModalCloseBtnCreateUser().click()

        // await browser.sleep(5000)

    
        } catch(e){
            console.log("Exception is:",e)
        }
    }
}
