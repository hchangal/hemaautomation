import { $, browser, ElementFinder, element, By, ExpectedConditions } from "protractor"

export class UpdateTracking {

    projectSelectorLocator: string = '#ctl00_ctl00_ctl00_baseBody_body_body_ddProject'
    // jdtypeSelector: ElementFinder
    eventSelectorLocator: string = '#ctl00_ctl00_baseBody_body_ddEventType'
    jdtTypeSelectorLocator: string = '#ctl00_ctl00_ctl00_baseBody_body_body_ddEntity'

    constructor() {

        // this.projectSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddProject')
        // this.jdtypeSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEntity')

    }
    goto() {
        browser.get('sci.framework.web/Tracking/BulkUploadTracking.aspx')

    }

    async getprojectSelector(value: string) {
        var sel = await $(this.projectSelectorLocator).element(By.xpath("//option[text()='" + value + "']"))
        var x = await browser.wait(ExpectedConditions.presenceOf(sel))
        await sel.click()
        // await $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEventType').sendKeys(value)
    }

    // getprojectSelector() {

    //     return $(this.projectSelectorLocator)
    // }

    getJdtTypeSelector() {
        return $(this.jdtTypeSelectorLocator)
    }

    async getEventSelector(value: string) {
        var sel = await element(By.xpath("//option[text()='" + value + "']"))
        await browser.wait(ExpectedConditions.presenceOf(sel))
        await sel.click()
        // await $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEventType').sendKeys(value)
    }

    async selectProject(project: string, jdt: string, event: string) {
        browser.getCurrentUrl().then(console.log);
        try {
            await this.getprojectSelector(project)
            await this.getJdtTypeSelector().element(By.xpath("//option[text()='" +jdt+ "']")).click()
            await this.getEventSelector(event)
        } catch (e) {
            console.log('Error is: ', e)
        }

    }
}
