import { $, browser, ElementFinder, element, By, ExpectedConditions } from "protractor"

export class UpdateDates {

    projectSelectorUd: string = '#ctl00_ctl00_baseBody_body_ddProject';
    // jdtypeSelector: ElementFinder
    eventSelectorLocatorUd: string = '#ctl00_ctl00_baseBody_body_ddEventType'
    jdtTypeSelectorLocatorUd: string = '#ctl00_ctl00_baseBody_body_ddEntity'

    constructor() {

        // this.projectSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddProject')
        // this.jdtypeSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEntity')

    }
    goto() {
        browser.get('sci.framework.web/Tracking/BulkUploadDates.aspx')

    }

    getprojectSlectorUd() {

        return $ (this.projectSelectorUd)
    }

    getJdtTypeSelectorUd() {
        return $(this.jdtTypeSelectorLocatorUd)
    }

    async getEventSelectorUd(value: string) {
        var sel = await element(By.xpath("//option[text()='" + value + "']"))
        var x = await browser.wait(ExpectedConditions.presenceOf(sel))
        await sel.click()
        // await $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEventType').sendKeys(value)
    }

    async selectProjectUd(project: string, jdt: string, event: string) {
        browser.getCurrentUrl().then(console.log);
        try {
            await this.getprojectSlectorUd().element(By.xpath("//option[text()='" +project+ "']")).click()
            await this.getJdtTypeSelectorUd().element(By.xpath("//option[text()='" +jdt+ "']")).click()
            await this.getEventSelectorUd(event)
            // await browser.sleep(10000)
        } catch (e) {
            console.log('Error is: ', e)
        }

    }
}