import { $, browser, ElementFinder } from "protractor"

export class Filestatuschange {

    jdtSelector: ElementFinder
    fileStatusSelector : ElementFinder

    constructor() {
        this.jdtSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_param0')
        this.fileStatusSelector = $('ctl00_ctl00_ctl00_baseBody_body_body_param2')
    }


    async determineFilestatus(doctype: string) {
        await this.jdtSelector.$("[value='" + doctype + "']").click()
        return this.fileStatusSelector.$$('option')
    }

}