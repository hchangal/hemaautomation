import { browser, element, by } from "protractor";

export default class Page {
    private title: string;

    constructor() {
        this.title = 'My Page';
    }

    open(path) {
        browser.get(path);
    }

    async menu(...menuItems) {
        for (var item in menuItems) {
            await element(by.linkText(menuItems[item])).click();
        }
    }
}