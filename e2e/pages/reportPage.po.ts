import { browser, element, By, ElementFinder, $, ExpectedConditions } from "protractor";
import Page from './page';

export class ReportPage extends Page {

    quickSearch: ElementFinder;
    selectBtn: ElementFinder;

    constructor() {
        super();
        this.quickSearch = $('input[value="Quick Search"]');
        this.selectBtn = $('input[value="Select"]');
    }

    open() {
        super.open('SCI.Framework.Web/Reports/ReportList.aspx');
    }

    on() {
        return browser.wait(ExpectedConditions.presenceOf(this.quickSearch));
    }

    async pickReport(value: string) {
        await this.quickSearch.sendKeys(value);
        await this.selectBtn.click();
    }

}