import { browser, ElementFinder, $, ExpectedConditions } from "protractor";

export class Login {
    userName: ElementFinder;
    passWord: ElementFinder
    submitButton: ElementFinder;

    constructor() {
        this.userName = $('input[name="ctl00$ctl00$baseBody$body$username"]')
        this.passWord = $('input[name="ctl00$ctl00$baseBody$body$password"]')
        this.submitButton = $('input[name="ctl00$ctl00$baseBody$body$btnLogin"]')
    }

    async goto(){
        await browser.get('sci.framework.web/Portal/External/Login.aspx');
    }

    async on() {
        await browser.wait(ExpectedConditions.presenceOf(this.userName))
    }

    async loginAs(user: string, pass: string) {
        await this.userName.sendKeys(user)
        await this.passWord.sendKeys(pass)
        await this.submitButton.click()
    }
}