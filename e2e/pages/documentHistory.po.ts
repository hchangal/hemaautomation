import { $,$$, browser, ElementFinder, element, By, ExpectedConditions, protractor, ElementArrayFinder } from "protractor"
import { Selector } from '../support/selector'
import { createPublicKey } from "crypto";



export class DocumentHistory {

    projectSelectDh: string = '#ctl00_ctl00_baseBody_body_cboProject';
    referenceSelect: string = '#ctl00_ctl00_baseBody_body_txtReference'
    fileTypeSelect: string = '#ctl00_ctl00_baseBody_body_cboFileType'
    docTypeSelect: string = '#ctl00_ctl00_baseBody_body_cboDocumentType'
    docClass: string = '#ctl00_ctl00_baseBody_body_cboDocumentType'
    historyBox: string = '#ctl00_ctl00_baseBody_body_tblHistory'
    


    constructor() {
    }
    goto() {

        browser.get('records/records%20Management/history_document.aspx')

    
  }
    getProjectSelectDh() {

        return $(this.projectSelectDh)
    }

    getreferenceSelect() {
        return $(this.referenceSelect)
    }

    getFileTypeSelect() {
        return $(this.fileTypeSelect)
    }

    getDocTypeSelect() {
        return $(this.docTypeSelect)
    }

    getDocClass() {
        return $(this.docClass)
    }

    getHistoryBox() {
        return $(this.historyBox)
    }



    async documentHistory(project: string, reference: string, fileType: string, docType: string , classType: string) {

        // try {
       
        var projectSelector: Selector = new Selector(this.getProjectSelectDh())
        await projectSelector.waitToSelectByContainsText(project)
        

        await this.getreferenceSelect().sendKeys(reference)

        await this.getProjectSelectDh().click()

        var fileTypeSelector: Selector = new Selector(this.getFileTypeSelect())
        await fileTypeSelector.waitToSelectByContainsText(fileType)
        await this.getFileTypeSelect().click()

        var docTypeSelector: Selector = new Selector(this.getDocTypeSelect())
        await docTypeSelector.waitToSelectByContainsText(docType)

        var docClassSelector: Selector = new Selector(this.getDocClass())
        await docClassSelector.waitToSelectByContainsText(classType)
        // await this.getDocTypeSelect().click()



        await browser.sleep(5000)

    
        
        // } catch(e){
        //     console.log("Exception is:",e)
        // }
    }

  
}