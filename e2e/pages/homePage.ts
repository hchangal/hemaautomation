import { $, ElementFinder, element, by, browser, ExpectedConditions } from "protractor";
import Page from "./page";

export class HomePage extends Page {
    public pageHeaderTitle: ElementFinder;

    constructor() {
        super();
        this.pageHeaderTitle = $('#page-header-title');
    }

    on() {
        return browser.wait(ExpectedConditions.presenceOf(this.pageHeaderTitle));
    }

}
