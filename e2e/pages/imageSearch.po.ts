import { $,$$, browser, ElementFinder, element, By, ExpectedConditions, protractor, ElementArrayFinder } from "protractor"
import { Selector } from '../support/selector'
import { createPublicKey } from "crypto";



export class ImageSearch {

    criteriaField: string = '#ctl00_ctl00_baseBody_body_txtValues';
    projectSelect: string = '#ctl00_ctl00_baseBody_body_cbProject'
    searchBtn: string = '#ctl00_ctl00_baseBody_body_btnSearch'
    tableImageSearch: string = '#ctl00_ctl00_baseBody_body_radGridSearch_ctl00'
    tpCheckBox: string = '#ctl00_ctl00_baseBody_body_radGridSearch_ctl00_ctl06_ClientSelectColumnSelectCheckBox'
    aomCheckBox: string = '#ctl00_ctl00_baseBody_body_radGridSearch_ctl00_ctl08_ClientSelectColumnSelectCheckBox'
    finalCheckBox: string = '#ctl00_ctl00_baseBody_body_radGridSearch_ctl00_ctl04_ClientSelectColumnSelectCheckBox'
    

 


    constructor() {
    }
    goto() {

        browser.get('sci.framework.web/ImageSearch/Search.aspx')

    
  }
    getCriteriaField() {

        return $(this.criteriaField)
    }

    getProjectSelect() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.projectSelect)));
        return $(this.projectSelect)
    }

    getSearchBtn() {
        return $(this.searchBtn)
    }

    getTableImageSearch() {
        return $(this.tableImageSearch)
    }
    getPdfMerge() {
        // return browser.element(By.css("a[title='Merge PDF']"))
        return $("a[title='Merge PDF']")
    }

    getTifMerge() {
        // return browser.element(By.css("a[title='Merge PDF']"))
        return $("a[title='Merge TIF']")
    }
    getAomCheckBox() {
        return $(this.aomCheckBox)
    }

    getTpCheckBox() {
        return $ (this.tpCheckBox)
    }

    getCsvFile() {
        return $("a[title='Export to CSV file']")
    }

    getFinalCheckBox() {
        return $(this.finalCheckBox)
    }

    async imageSearch(criteria: string, project: string) {

        

        await this.getCriteriaField().sendKeys(criteria)
        await browser.sleep(1000)
       
        var projectSelector: Selector = new Selector(this.getProjectSelect())
        await projectSelector.waitToSelectByContainsText(project)
        await browser.sleep(1000)
        
        await this.getSearchBtn().click()

        await browser.sleep(3000)

    }

    async imageSearchCheck(jdt: string) {

        var linkJdt = element(By.linkText(jdt))
        
        await browser.wait(ExpectedConditions.presenceOf(linkJdt),5000)
      
        await linkJdt.click()

        var handles = await browser.getAllWindowHandles()
        console.log(handles)

        await browser.switchTo().window(handles[1])
        await browser.sleep(5000)
        await browser.close()
        await browser.switchTo().window(handles[0])
        

        // await this.getAomCheckBox().click()
        // await this.getTpCheckBox().click()
        // await this.getPdfMerge().click()
        

        // var handles = await browser.getAllWindowHandles()
        // console.log(handles)

        // await browser.switchTo().window(handles[1])
        // await browser.sleep(5000)
        // await browser.close()
        // await browser.switchTo().window(handles[0])
    }

    async imageMerge() {

        await this.getFinalCheckBox().click()
        await this.getTpCheckBox().click()
        await this.getPdfMerge().click()
        

        var handles = await browser.getAllWindowHandles()
        console.log(handles)

        await browser.switchTo().window(handles[1])
        await browser.sleep(5000)
        await browser.close()
        await browser.switchTo().window(handles[0])
    }

    async imageMergeTif() {

      await this.getFinalCheckBox().click()
      await this.getTpCheckBox().click()
      await this.getTifMerge().click()
      

      var handles = await browser.getAllWindowHandles()
      console.log(handles)

      await browser.switchTo().window(handles[1])
      await browser.sleep(5000)
      await browser.close()
      await browser.switchTo().window(handles[0])
  }

  async imageCsvDownload() {

    await this.getCsvFile().click()
  
}


        

        
        



     

    
        
      
    }

  
