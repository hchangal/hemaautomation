import { $, browser, ElementFinder, element, By, ExpectedConditions, protractor, } from "protractor"
import { Selector } from '../support/selector'
import * as path from 'path'



export class UploadImages {


    submissionCategory: string = '#ctl00_ctl00_ctl00_baseBody_body_body_docType'
    uploadFileSelect: string = '#ctl00_ctl00_ctl00_baseBody_body_body_uploadfile0'
    uploadImageFile: string = '#ctl00_ctl00_ctl00_baseBody_body_body_btnUpload'
    dialogWindow: string = '#dialogWindowTitle'
    modalClosebtn: string = '.btn.btn-primary'

    // #dialogWindow > div > div > div.modal-footer > button
                            

    constructor() {

        // this.projectSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddProject')
        // this.jdtypeSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEntity')
        // this.resultsTable = $('.rgMasterTable');
        // this.submissionCategory = $('ctl00_ctl00_ctl00_baseBody_body_body_docType')

    }


    getSubmissionCategory() {
        return $(this.submissionCategory)
    }
e
    getuploadFileSelect() {
        return $(this.uploadFileSelect)
    }

    getuploadImageFile() {
        return $(this.uploadImageFile)
    }

    getmodalCloseBtn() {
       return element(By.xpath("//label[text()='Uploaded']/../../..//button"))

        //*[@id="dialogWindow"]/div/div/div[3]/button
        //*[@id="dialogWindowTitle"]
        // return $(this.modalClosebtn)
    }

    getDialogWindow() {
        return $(this.dialogWindow)
    }

    async imageSearch1(submission: string) {

        try {
            
            let remote = require('selenium-webdriver/remote');
            browser.setFileDetector(new remote.FileDetector());

            const fileToUpload = '../../data/samplePdfFile.pdf'
            const completeFilePath = path.resolve(__dirname, fileToUpload);
            console.log(completeFilePath)
            await this.getSubmissionCategory().element(By.xpath("//option[text()='" + submission + "']")).click()

            await this.getuploadFileSelect().sendKeys(path.resolve(completeFilePath))

            await browser.sleep(3000)

            
            await this.getuploadImageFile().click()
            await browser.sleep(1000) 
            // await this.getmodalCloseBtn().click()
            // await browser.sleep(1000)
            

            


        } catch (e) {
            console.log('Error is:', e)
        }






    }
}