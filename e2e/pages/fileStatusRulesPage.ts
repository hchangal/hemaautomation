import Page from './page';
import { ElementFinder, $, browser, ExpectedConditions, By } from 'protractor';

export class FileStatusRulesPage extends Page {

    jdtDropDown: ElementFinder;
    actionDropDown: ElementFinder;
    fileStatusDropDown: ElementFinder;

    constructor(){
        super();
        this.jdtDropDown = $('#ctl00_ctl00_ctl00_baseBody_body_body_param0');
        this.actionDropDown = $('#ctl00_ctl00_ctl00_baseBody_body_body_param1');
        this.fileStatusDropDown = $('#ctl00_ctl00_ctl00_baseBody_body_body_param2');
    }

    on() {
        return browser.wait(ExpectedConditions.presenceOf(this.jdtDropDown));
    }

    selectJdt(value: string) {
        this.jdtDropDown.element(By.xpath("//option[text()='"+value+"']")).click();
    }
}