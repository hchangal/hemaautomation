import { $, browser, ElementFinder, element, By, ExpectedConditions, protractor } from "protractor"
import { Selector } from '../support/selector'



export class MergePrintJDT {

    projectSelectorPrintJDT: string = '#ctl00_ctl00_baseBody_body_ddlProject';
    printDocType: string = '#ctl00_ctl00_baseBody_body_ddlEntityDocType'
    printBlank: string = '#ctl00_ctl00_baseBody_body_chkBlankSignorInfo'
    loanNumbers: string = '#ctl00_ctl00_baseBody_body_txtLoans'
    generateBtn: string = '#ctl00_ctl00_baseBody_body_btnGenerate'
    outputFormat: string = '#ctl00_ctl00_baseBody_body_ddlOutputFormat'

    constructor() {

        // this.projectSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddProject')
        // this.jdtypeSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEntity')

        // this.submissionCategory = $('ctl00_ctl00_ctl00_baseBody_body_body_docType')

    }
    goto() {
        browser.get('sci.framework.web/DataMerge/DataMerge.aspx')

    }

    getprojectSlectorPrintJDT() {

        return $(this.projectSelectorPrintJDT)
    }

    getprintDocType() {
        return $(this.printDocType)
    }

    getprintBlank() {
        return $(this.printBlank)
    }

    getloanNumbers() {
        return $(this.loanNumbers)
    }

    getgenerateBtn() {
        return $(this.generateBtn)
    }

    getoutputFormat() {

        return $(this.outputFormat)
    }

    async mergePrintJDT(project: string, printDocType: string, loanNumber: string, ) {

        try {


            // await this.getprojectSlectorPrintJDT().element(By.xpath("//option[text()='" + project + "']")).click()
            // await this.getprojectSlectorPrintJDT().click()
            var selector: Selector = await new Selector(this.getprojectSlectorPrintJDT());
            await selector.selectByContainsText(project)

            //div[./div/div[normalize-space(.)='More Actions...']]
            var selector: Selector = await new Selector(this.getprintDocType());
            // await selector.selectByContainsText(printDocType);
            await browser.sleep(3000);
            await selector.optionByWhiteSpaceNormalized(printDocType).click();
            // await browser.sleep(10000)
            await this.getprintBlank().click();
            await browser.sleep(5000)
            await browser.switchTo().alert().accept();
            // var enter = await browser.actions().sendKeys(protractor.Key.ENTER);
            // await enter.perform();
            await browser.sleep(5000)
            await this.getloanNumbers().sendKeys(loanNumber);
            await browser.sleep(3000)
            // var element = await element(By.tagName('body'))
            // await browser.executeScript("arguments[0].click()", element)
            // var elm = element(by.css('.material-dialog-container'));
            // await browser.actions()
            //     .mouseMove(element, {x: 10, y: 10})
            //     .click()
            //     .perform();


            // input_field = browser.find_element(:xpath, '/html/body/div[5]/div/div[3]/div[2]/div[2]/div/div/div/div/div/div/input')
            await browser.executeScript('arguments[0].removeAttribute("disabled");', this.getgenerateBtn().getWebElement())

            // const plot0 = await $('#divBody > div.container-fluid > div.row')
            // await browser.actions()
            //     .mouseMove(plot0, { x: 80, y: 45 })
            //     .mouseDown()
            //     .mouseMove({ x: 0, y: 25 })
            //     .perform();
            // var selector: Selector = await new Selector(this.getoutputFormat());
            // await selector.selectByContainsText(PDF)
            await browser.sleep(3000)
            await this.getgenerateBtn().click()
            await browser.sleep(20000)


            // await browser.sleep(3000) 
            // var link = await element(By.linkText("2"))
            // await browser.wait(ExpectedConditions.presenceOf(link))
            // await link.click()



        } catch (e) {
            console.log('Error is:', e)
        }






    }
}