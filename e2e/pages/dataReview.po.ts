import { $, browser, ElementFinder, element, By, ExpectedConditions, protractor } from "protractor"
import { Selector } from '../support/selector'



export class DataReview {

    projectSelectorDataReview: string = '#cboProject';
    referenceNumDataReview: string = '#txtReference'
    documentDataReview: string = '#cboDocument'
    addressField: string = '#control135'
    submitBtnDataReview: string = '#ctl00_ctl00_baseBody_body_btnSubmit'
    legalDesText: string = '#control150'
    dataSavedText: string = '#ctl00_ctl00_baseBody_body_lblError'


    constructor() {


    }
    goto() {
        browser.get('Custodial/Review/Review.aspx')

    }

    getProjectSlectorDataReview() {

        return $(this.projectSelectorDataReview)
    }

    getReferenceNumDataReview() {
        return $(this.referenceNumDataReview)
    }

    getDocumentDataReview() {
        return $(this.documentDataReview)
    }

    getAddressField() {
        return $(this.addressField)
    }

    getSubmitBtnDataReview() {
        return $(this.submitBtnDataReview)
    }

    getLegalDesText() {
        return $(this.legalDesText)
    }

    getDataSavedText() {
        return $(this.dataSavedText)
    }
   

    async dataReviewChangeAddress(project: string, reference: string, document: string, address: string) {
        var projectSelector: Selector = new Selector(this.getProjectSlectorDataReview());
        await projectSelector.waitToSelectByContainsText(project)
      
        await this.getReferenceNumDataReview().sendKeys(reference)
        await this.getProjectSlectorDataReview().click()

        var documentSelector: Selector = new Selector(this.getDocumentDataReview());
        await documentSelector.waitToSelectByWhiteSpaceNormalized(document)

        await this.getAddressField().clear
        await this.getAddressField().sendKeys(address)

        await this.getSubmitBtnDataReview().click()
        await browser.sleep(3000)
    }

    async dataReviewChangeLegalDescription(project: string, reference: string, document: string, legaldes: string) {
        var projectSelector: Selector = new Selector(this.getProjectSlectorDataReview());
        await projectSelector.waitToSelectByContainsText(project)
      
        await this.getReferenceNumDataReview().clear()
        await this.getReferenceNumDataReview().sendKeys(reference)
        await this.getProjectSlectorDataReview().click()

        var documentSelector: Selector = new Selector(this.getDocumentDataReview());
        await documentSelector.waitToSelectByWhiteSpaceNormalized(document)

        await this.getLegalDesText().clear ()
        await this.getLegalDesText().sendKeys(legaldes)

        await this.getSubmitBtnDataReview().click()
        // await this.getMergeBtn().sendKeys(protractor.Key.ENTER)
    }
}