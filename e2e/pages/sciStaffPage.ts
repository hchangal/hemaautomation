import { $, ElementFinder, element, by, browser, ExpectedConditions, $$, ElementArrayFinder } from "protractor";
import Page from "./page";

export class SciStaffPage extends Page {

    public images: ElementArrayFinder;

    constructor() {
        super();
        this.images = $$('.sciImage');
    }

    open() {
        super.open('/SCI.Framework.Web/Reports/ReportList.aspx');
    }

    /**
     * hdworin - hdworin@firstam.com
     * 
     * on is noop because images is the only thing used on page and
     * should not need load protection because  it is ElementFinderArray 
     * instead of ElementFinder
     */
    on() {
        
    }

    returnFirstImage() {
        return $$('.sciImage').first();
    }

}

function presenceOfAll(elementArrayFinder) {
    return function () {
        return elementArrayFinder.count(function (count) {
            return count > 0;
        });
    };
}