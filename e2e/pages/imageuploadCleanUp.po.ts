
import { $,$$, browser, ElementFinder, element, By,by, ExpectedConditions, } from "protractor"
import { Selector } from '../support/selector'




export class  SearchUploadImagesCleanup {

    projectSelectorImageU: string = '#ctl00_ctl00_ctl00_baseBody_body_body_ddProject';
    // jdtypeSelector: ElementFinder
    accountNumberImageU: string = '#ctl00_ctl00_ctl00_baseBody_body_body_txtReference'
    SearchButtonImageU: string = '#ctl00_ctl00_ctl00_baseBody_body_body_btnFind'
    resultsTable: ElementFinder
    uploadTable: string = ('#ctl00_ctl00_ctl00_baseBody_body_body_gvData')
    uploadList: string = '#ctl00_ctl00_ctl00_baseBody_body_body_gvData'

    imamgeList: ElementFinder;

    constructor() {

        this.imamgeList = $('ctl00_ctl00_ctl00_baseBody_body_body_gvData');
        // this.projectSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddProject')
        // this.jdtypeSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEntity')
        // this.submissionCategory = $('ctl00_ctl00_ctl00_baseBody_body_body_docType')

    }
    goto() {

        browser.get('sci.framework.web/ImageHistory/ImageHistory.aspx')

    }

    getprojectSlectorImageU() {
        browser.wait(ExpectedConditions.presenceOf($(this.projectSelectorImageU)))
        return $(this.projectSelectorImageU)
    }
    

    getaccountNumberImageU() {
        return $(this.accountNumberImageU)
    }

    getSearchButtonImageU() {
        return $(this.SearchButtonImageU)
    }

    getuploadTable() {
        return $(this.uploadTable)

    }


    getuploadList() {
        return $$(this.uploadList)

    }

    selectUploads() {
        return this.imamgeList.element(By.xpath("//input[contains(@name,'imgDisabled')]")).click();
    }


    async imageSearchCleanup(project: string, accountNumber: string, ) {

        try {

            // await this.getprojectSlectorImageU().element(By.xpath("//option[text()='" + project + "']")).click()
            await this.getprojectSlectorImageU().click()
            let TTproject = await this.getprojectSlectorImageU();
            var selector: Selector = new Selector(TTproject);
            await selector.selectByText(project);
            await this.getaccountNumberImageU().sendKeys(accountNumber);
            await browser.sleep(3000)
            await this.getSearchButtonImageU().click();
            await browser.sleep(3000) 

 
            await element.all(By.xpath("//input[contains(@name,'imgDisabled')]")).each(function(elements) {
                browser.sleep(1000) 
                elements.click();   
                    browser.sleep(2000)  
                    console.log('test2')

              });




        } catch (e) {
            console.log('Error is:', e)
        }






    }
}