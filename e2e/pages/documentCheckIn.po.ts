import { $, $$, browser, ElementFinder, element, By, ExpectedConditions, protractor, ElementArrayFinder } from "protractor"
import { Selector } from '../support/selector'
import { createPublicKey } from "crypto";



export class DocumentCheckIn {

    projectSelectCheckIn: string = '#ctl00_ctl00_baseBody_body_cbProject';
    referenceCheckIn: string = '#ctl00_ctl00_baseBody_body_txtLoans'
    searchBtn: string = '#ctl00_ctl00_baseBody_body_btnSearchLoans'
    loanCheckBox: string = '#ctl00_ctl00_baseBody_body_gridResults_ctl02_chkRow'
    batchName: string = '#ctl00_ctl00_baseBody_body_txtBatch'
    locationField: string = '#ctl00_ctl00_baseBody_body_txtLocation'
    fileTypeSelect: string = '#ctl00_ctl00_baseBody_body_cbFileType'
    docTypeSelect: string = '#ctl00_ctl00_baseBody_body_cbDocType'
    classSelect: string = '#ctl00_ctl00_baseBody_body_cbClass'
    addbtn: string = '#ctl00_ctl00_baseBody_body_btnAdd'
    //    ctl00_ctl00_baseBody_body_btnAdd
    submitBtn: string = '#ctl00_ctl00_baseBody_body_btnSubmit'
    slipSheet: string = '#ctl00_ctl00_baseBody_body_cbMergeProcess'
    generateBtn: string = '#ctl00_ctl00_baseBody_body_btnSlipSheet'
    loanClassSelector: string = '#ctl00_ctl00_baseBody_body_gridSubmit_ctl00_ctl04_cbClassType'
    pdfToolbar: string = 'viewer-pdf-toolbar'


    constructor() {
    }
    async goto() {
        await browser.get('sci.framework.web/DocCheckIn/DocCheckIn.aspx')
    }

    getProjectSelectCheckIn() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.projectSelectCheckIn)));

        return $(this.projectSelectCheckIn)
    }

    getReferenceCheckIn() {
        browser.sleep(1000)
        browser.wait(ExpectedConditions.presenceOf($(this.referenceCheckIn)));
        return $(this.referenceCheckIn)
    }

    getSearchBtn() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.searchBtn)));
        browser.sleep(1000)
        return $(this.searchBtn)
    }

    getLoanCheckBox() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.loanCheckBox)));
        return $(this.loanCheckBox)
    }

    getBatchName() {
        browser.wait(ExpectedConditions.presenceOf($(this.batchName)));
        return $(this.batchName)
    }

    getLocationField() {
        browser.wait(ExpectedConditions.presenceOf($(this.locationField)));
        return $(this.locationField)
    }

    getFileTypeSelect() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.fileTypeSelect)));
        return $(this.fileTypeSelect)
    }

    getDocTypeSelect() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.docTypeSelect)));
        return $(this.docTypeSelect)
    }

    getClassSelect() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.classSelect)));
        return $(this.classSelect)
    }

    getAddbtn() {
        var elem = $(this.addbtn)
        return browser.executeScript('window.scrollTo(0,0);')
        .then(function(){
            return elem
        })
    }

    getSubmitBtn() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.submitBtn)));
        return $(this.submitBtn)
    }

    getSlipSheet() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.slipSheet)));
        return $(this.slipSheet)
    }

    getGenerateBtn() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.generateBtn)));
        return $(this.generateBtn)
    }

    getLoanClassSelector() {
        browser.wait(ExpectedConditions.elementToBeClickable($(this.loanClassSelector)));
        return $(this.loanClassSelector)

        
    }

    getPdfToolbar() {
    return $(this.pdfToolbar)
    // return browser.findElement(By.tagName(this.pdfToolbar))
        
    }

    getIsPdf() {
        return $("embed[type='application/pdf']")
    }

    async documentCheckIn(project: string, reference: string, batch: string, location: string, fileType: string, classType: string, loanClass: string, slip: string) {
        // try {

        var projectSelector: Selector = new Selector(this.getProjectSelectCheckIn())
        await projectSelector.waitToSelectByContainsText(project)
        await browser.sleep(3000)
        await this.getReferenceCheckIn().sendKeys(reference)
        
        await this.getSearchBtn().click()
        await browser.sleep(3000)

        await this.getLoanCheckBox().click()

        await this.getBatchName().sendKeys(batch)
        await browser.sleep(1000)
        await this.getLocationField().sendKeys(location)

        var fileTypeSelector: Selector = new Selector(this.getFileTypeSelect())
        await fileTypeSelector.waitToSelectByContainsText(fileType)

        await browser.sleep(1000)

        var docTypeSelector: Selector = new Selector(this.getDocTypeSelect())
        await docTypeSelector.waitToSelectByContainsText("Assignment of Mortgage")

        await browser.sleep(1000)
        var docClassSelector: Selector = new Selector(this.getClassSelect())
        await docClassSelector.waitToSelectMultipleDisabledOptionsText(classType)
        // await browser.sleep(6000)
        var addBtn = await this.getAddbtn()
        await addBtn.click()
        await browser.sleep(1000)

        var loanClassSelector: Selector = new Selector(this.getLoanClassSelector())
        await loanClassSelector.waitToSelectMultipleDisabledOptionsText(loanClass)

        // await browser.sleep(5000)

        // browser.manage().window().getSize().then(function(size) {
        //     // size is still an unresolved promise ;.;
        //     console.log(size)
        //   });
        var size = await browser.manage().window().getSize();
          await browser.sleep(5000)
        await this.getSubmitBtn().click()

        await browser.executeScript('window.scrollTo('+size.height+','+size.width+');');
     

            

        await browser.sleep(5000)

        var slipSheetSelector: Selector = new Selector(this.getSlipSheet())
        await slipSheetSelector.waitToSelectMultipleDisabledOptionsText(slip)
        // await browser.sleep(5000)
        await this.getGenerateBtn().click()
        let x = await this.getGenerateBtn().getLocation()
        
        console.log('x is :', x )
        

    // } catch (e) {
    //     console.log('Error is:', e)
    // }

          await browser.sleep(4000)
        var stuff = await browser.getAllWindowHandles();
        console.log("Number of handles:",stuff.length)

        await browser.wait(this.windowCount(2), 10000)

        var handles = await browser.getAllWindowHandles()

        await browser.switchTo().window(handles[1])
        
        await browser.sleep(9000)
        // console.log(await browser.getPageSource())
        // console.log('handle is', handles[1])
        // await this.getPdfToolbar().getTagName()
        await this.getIsPdf().getTagName()
        // console.log('handle is', handles[1])
        await browser.close()
        await browser.switchTo().window(handles[0])
        // await browser.sleep(30000)



        

    }

    windowCount(count: number) {
        return async function () {
            const handles = await browser.getAllWindowHandles();
            return handles.length === count;
        };
    };


}