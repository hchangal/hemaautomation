import { $, browser, ElementFinder, element, By, ExpectedConditions, } from "protractor"
import { Selector } from '../support/selector'



export class SearchUploadImages {

    projectSelectorImageU: string = '#ctl00_ctl00_ctl00_baseBody_body_body_ddProject';
    // jdtypeSelector: ElementFinder
    accountNumberImageU: string = '#ctl00_ctl00_ctl00_baseBody_body_body_txtLoanNumber'
    SearchButtonImageU: string = '#ctl00_ctl00_ctl00_baseBody_body_body_btnSearch'
    resultsTable: ElementFinder
    submissionCategory: string = ('ctl00_ctl00_ctl00_baseBody_body_body_docType')
    dialogWindow: string = ('#dialogWindowTitle')

    constructor() {

        // this.projectSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddProject')
        // this.jdtypeSelector = $('#ctl00_ctl00_ctl00_baseBody_body_body_ddEntity')
        this.resultsTable = $('.rgMasterTable');
        // this.submissionCategory = $('ctl00_ctl00_ctl00_baseBody_body_body_docType')

    }
    goto() {

        browser.get('sci.framework.web/ImageEventUpload/Search/Search.aspx')

    }

    getprojectSlectorImageU() {
        browser.wait(ExpectedConditions.presenceOf($(this.projectSelectorImageU)))
        return $(this.projectSelectorImageU)
    }

    getaccountNumberImageU() {
        return $(this.accountNumberImageU)
    }

    getSearchButtonImageU() {
        return $(this.SearchButtonImageU)
    }

    getSubmissionCategory() {
        return $(this.submissionCategory)

    }

    getDialogWindo() {
        return $(this.dialogWindow)
    }

    async imageSearch(project: string, accountNumber: string, ) {

        try {

            // await this.getprojectSlectorImageU().element(By.xpath("//option[text()='" + project + "']")).click()
            await this.getprojectSlectorImageU().click()
            let TTproject = await this.getprojectSlectorImageU();
            var selector: Selector = new Selector(TTproject);
            await selector.selectByText(project);
            await this.getaccountNumberImageU().sendKeys(accountNumber);
            await browser.sleep(3000)
            await this.getSearchButtonImageU().click();
            await browser.sleep(3000) 

    
            var link = await element(By.linkText(accountNumber))
            await browser.wait(ExpectedConditions.presenceOf(link))
            await link.click()

            


        } catch (e) {
            console.log('Error is:', e)
        }






    }
}