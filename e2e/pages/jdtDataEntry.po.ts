import { $, browser, ElementFinder, element, By, ExpectedConditions, protractor } from "protractor"
import { Selector } from '../support/selector'



export class JdtDataEntry {

    projectSelectorJdtDataEntry: string = '#ctl00_ctl00_ctl00_baseBody_body_body_ddlProjects';
    referenceNum: string = '#ctl00_ctl00_ctl00_baseBody_body_body_txtRefNum'
    jdtTypeDataEntry: string = '#ctl00_ctl00_ctl00_baseBody_body_body_ddlEntityDocType'
    propertyLegalDes: string = '#SCI_DF_150'
    submitBtnJdtDataEntry: string = '#ctl00_ctl00_ctl00_baseBody_body_body_btnSubmit'


    constructor() {


    }
    goto() {
        browser.get('AssignmentRelease/DataEntry_Global.aspx')

    }

    getProjectSlectorJdtDataEntry() {

        return $(this.projectSelectorJdtDataEntry)
    }

    getReferenceNum() {
        return $(this.referenceNum)
    }

    getJdtTypeDataEntry() {
        return $(this.jdtTypeDataEntry)
    }

    getPropertyLegalDes() {
        return $(this.propertyLegalDes)
    }

    getSubmitBtnJdtDataEntry() {
        return $(this.submitBtnJdtDataEntry)
    }

    async jdtDataEntry(project: string, reference: string, jdt: string, legaltxt: string) {
        var projectSelector: Selector = new Selector(this.getProjectSlectorJdtDataEntry());
        await projectSelector.waitToSelectByContainsText(project)
      
        await this.getReferenceNum().sendKeys(reference)
        await this.getProjectSlectorJdtDataEntry().click()

        var jdtSelector: Selector = new Selector(this.getJdtTypeDataEntry());
        await jdtSelector.waitToSelectByWhiteSpaceNormalized(jdt)

        await this.getPropertyLegalDes().sendKeys(legaltxt);

        await this.getSubmitBtnJdtDataEntry().click()
        // await this.getMergeBtn().sendKeys(protractor.Key.ENTER)
    }
}