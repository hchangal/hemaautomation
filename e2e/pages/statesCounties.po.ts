import { $,$$, browser, ElementFinder, element, By, ExpectedConditions, protractor, ElementArrayFinder } from "protractor"
import { Selector } from '../support/selector'



export class StateCounty {

    typeSelect: string = '#ctl00_ctl00_baseBody_body_cboType';
    parentSelect: string = '#ctl00_ctl00_baseBody_body_cboParent'
    countySelect: string = '#ctl00_ctl00_baseBody_body_cboEntity'
    jdtDataEntryReqs: string = '#ctl00_ctl00_baseBody_body_liEntityReqs'
    jdtTypeSc: string = '#ctl00_ctl00_baseBody_body_cboEntityReqsEntityDocType'
    dataEntryReqXml: string = '#ctl00_ctl00_baseBody_body_txtEntityReqs'
    saveBtnSc: string = '#ctl00_ctl00_baseBody_body_btnSaveEntityReqs'


    constructor() {
    }
    goto() {

        browser.get('assignmentrelease/county/setup.aspx')
  }
    getTypeSelect() {

        return $(this.typeSelect)
    }

    getParentSelect() {
        return $(this.parentSelect)
    }

    getCountySelect() {
        return $(this.countySelect)
    }

    getJdtTypeSc() {
        return $(this.jdtTypeSc)
    }

    getSaveBtnSc() {
        return $(this.saveBtnSc)
    }

    getJdtDataEntryReqs() {
        return $(this.jdtDataEntryReqs)
    }
    
    getDataEntryReqXml() {
        return $(this.dataEntryReqXml)
    }

    async stateCounty(states: string, county: string, jdt: string) {

        try {
        var parent = await this.getParentSelect()
        var statesSelector: Selector = new Selector(parent)
        await statesSelector.waitToSelectMultipleDisabledOptionsText(states)
        await this.getParentSelect().click()

        var countytSelector: Selector = new Selector(this.getCountySelect())
        await countytSelector.waitToSelectByContainsText(county)

        await this.getJdtDataEntryReqs().click()

        var jdtTypeScSelector: Selector = new Selector(this.getJdtTypeSc())
        await jdtTypeScSelector.waitToSelectMultipleDisabledOptionsText(jdt)

        await browser.sleep(5000)

        await this.getDataEntryReqXml().clear()
        await this.getDataEntryReqXml().sendKeys('wip')
    //     await this.getDataEntryReqXml().sendKeys("<EXPRESSION _Type="And">
    //     <REQUIREMENT _Operator="HasValue">
    //         <OPERAND _Type="DataField" _DataFieldId="253"/>
    //     </REQUIREMENT>
    // </EXPRESSION>")
        //   var xmlTextArea = driver.findElement(By.Css(this.jdtDataEntryReqs));
        //   driver.executeScript("arguments[0].setAttribute('value', arguments[1])", xmlTextArea, newValue);
        
        } catch(e){
            console.log("Exception is:",e)
        }
    }

  
}