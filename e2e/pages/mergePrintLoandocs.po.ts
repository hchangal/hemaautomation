import { $, browser, ElementFinder, element, By, ExpectedConditions, protractor } from "protractor"
import { Selector } from '../support/selector'



export class MergePrintLoanDocs {

    projectSelectorLoanDocs: string = '#ctl00_ctl00_baseBody_body_ddlProject';
    mergeProcess: string = '#ctl00_ctl00_baseBody_body_ddlMergeProcess'
    loansDocs: string = '#ctl00_ctl00_baseBody_body_txtLoans'
    mergeBtn: string = '#ctl00_ctl00_baseBody_body_btnMerge'
    errorMsg: string = '#ctl00_ctl00_baseBody_body_lblMessage'

    constructor() {


    }
    goto() {
        browser.get('sci.framework.web/DataMerge/MergeProcessDataMerge.aspx')

    }

    getProjectSlectorLoanDocs() {

        return $(this.projectSelectorLoanDocs)
    }

    getMergeProcess() {
        return $(this.mergeProcess)
    }

    getLoansDocs() {
        return $(this.loansDocs)
    }

    getMergeBtn() {
        return $(this.mergeBtn)
    }

    getErrorMsg() {
        return $(this.errorMsg)
    }

    async mergePrintLoanDocs(project: string, mergeProcess: string, loanNumber: string, ) {
        var projectSelector: Selector = new Selector(this.getProjectSlectorLoanDocs());
        await projectSelector.waitToSelectByContainsText(project)
        var mergeSelector: Selector = new Selector(this.getMergeProcess());
        await mergeSelector.waitToSelectByWhiteSpaceNormalized(mergeProcess)
        // await mergeSelector.waitToSelectByWhiteSpaceNormalized(mergeProcess)
        await this.getLoansDocs().sendKeys(loanNumber);
        await this.getMergeBtn().click()
        // await this.getMergeBtn().sendKeys(protractor.Key.ENTER)
    }
}