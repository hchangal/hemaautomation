import { $, browser, ElementFinder, element, By, ExpectedConditions, protractor } from "protractor"
import { Selector } from '../support/selector'
import { Driver } from "selenium-webdriver/chrome";
import { ReportPage } from '../pages/reportPage.po'
import { FileStatusRulesPage } from '../pages/fileStatusRulesPage'

const reports: ReportPage = new ReportPage();
const filestatusRule: FileStatusRulesPage = new FileStatusRulesPage();

export class AsmtCountyOverDue {

    projectSelectorAsmt: string = '#ctl00_ctl00_ctl00_baseBody_body_body_param0';
    jdtTypeAsmt: string = '#ctl00_ctl00_ctl00_baseBody_body_body_param1'
    selectExcel: string = '#ctl00_ctl00_ctl00_baseBody_body_body_rExcel'
    selectCsv: string = '#ctl00_ctl00_ctl00_baseBody_body_body_rCSV'
    generateBtn: string = '#ctl00_ctl00_ctl00_baseBody_body_body_btnGenerate'

    constructor() {


    }
    goto() {
        browser.get('SCI.Framework.Web/Reports/ReportList.aspx')

    }

    getProjectSlectorAsmt() {

        return $(this.projectSelectorAsmt)
    }

    getJdtTypeAsmt() {
        return $(this.jdtTypeAsmt)
    }

    getSelectExcel() {
        return $(this.selectExcel)
    }

    getSelectCsv() {
        return $(this.selectCsv)
    }

    getGenerateBtn() {
        return $(this.generateBtn)
    }

    // async function () {
    //     await reports.open();
    //     await reports.on();
    //     await reports.pickReport('ASMT - County Overdue [Mortgage Services][Assignment JDTs]');
    //     await filestatusRule.on();
    // }

    async asmtCountyOverduehtml(project: string, jdt: string) {
        var projectSelector: Selector = new Selector(this.getProjectSlectorAsmt());
        await projectSelector.waitToSelectByContainsText(project)
        var jdtSelector: Selector = new Selector(this.getJdtTypeAsmt());
        await jdtSelector.waitToSelectByWhiteSpaceNormalized(jdt)
        // await mergeSelector.waitToSelectByWhiteSpaceNormalized(mergeProcess)
        // await this.getLoansDocs().sendKeys(loanNumber);
        await this.getGenerateBtn().click()
        await browser.sleep(5000)
        var handles = await browser.getAllWindowHandles()
        await browser.switchTo().window(handles[1])
        await browser.close()
        await browser.switchTo().window(handles[0])

    }

async asmtCountyOverdueExcel(project: string, jdt: string) {
    var projectSelector: Selector = new Selector(this.getProjectSlectorAsmt());
    await projectSelector.waitToSelectByContainsText(project)
    var jdtSelector: Selector = new Selector(this.getJdtTypeAsmt());
    await jdtSelector.waitToSelectByWhiteSpaceNormalized(jdt)
    // await mergeSelector.waitToSelectByWhiteSpaceNormalized(mergeProcess)
    // await this.getLoansDocs().sendKeys(loanNumber);
    await this.getSelectExcel().click()
    await this.getGenerateBtn().click()
    // await this.getMergeBtn().sendKeys(protractor.Key.ENTER)
}

async asmtCountyOverdueCsv(project: string, jdt: string) {
    var projectSelector: Selector = new Selector(this.getProjectSlectorAsmt());
    await projectSelector.waitToSelectByContainsText(project)
    var jdtSelector: Selector = new Selector(this.getJdtTypeAsmt());
    await jdtSelector.waitToSelectByWhiteSpaceNormalized(jdt)
    // await mergeSelector.waitToSelectByWhiteSpaceNormalized(mergeProcess)
    // await this.getLoansDocs().sendKeys(loanNumber);
    await this.getSelectCsv().click()
    await this.getGenerateBtn().click()
    // await this.getMergeBtn().sendKeys(protractor.Key.ENTER)
}
}