const { BeforeAll, After, AfterAll, Status } = require("cucumber");
import * as fs from "fs";
import { browser } from "protractor";
import { Before } from "cucumber";



BeforeAll({ timeout: 100 * 1000 }, async () => {
    await browser.get('/');
});

Before(async function(){
    await browser.manage().deleteAllCookies();
})

After(async function (scenario) {
    if (scenario.result.status === Status.FAILED) {
        // screenShot is a base-64 encoded PNG
        const screenShot = await browser.takeScreenshot();
        this.attach(screenShot, "image/png");
    }
});

AfterAll({ timeout: 100 * 1000 }, async () => {
    await browser.quit();
});
