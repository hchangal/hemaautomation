
var fs = require('fs-extra');
var path = require('path');
var moment = require('moment');

module.exports = function () {
    var outputDir = __dirname + '/../../reports';
    if (fs.existsSync(outputDir)) {
        fs.moveSync(outputDir, outputDir + '_' + moment().format('YYYYMMDD_HHmmss') + "_" + Math.floor(Math.random() * 10000), {
            overwrite: true
        });
    }
    fs.mkdirSync(outputDir);
};