import { ElementFinder, By, browser, ExpectedConditions } from "protractor";

export class Selector {

    webelement: ElementFinder

    constructor(webelement: ElementFinder) {
        webelement.getTagName().then(function (type: string) {
            if (type != 'select') {
                throw Error('ElementTypeError: Webelement must be of type select')
            }
        })
        this.webelement = webelement
    }

    optionByText(text: string) {
        return this.webelement.element(By.xpath("//option[text()='" + text + "']"))
    }

    optionByContainsText(text: string) {
        return this.webelement.element(By.xpath("//option[contains(text(),'" + text + "')]"))
    }

    optionByWhiteSpaceNormalized(text: string) {
        return this.webelement.element(By.xpath("//option[normalize-space()='" + text + "']"))
    }

    multipleDisabledOptionsText(text: string) {
        return this.webelement.all(By.xpath("//option[.='" + text + "']")).filter(function(elem){
            return elem.isDisplayed();
        })
    }

    selectMultipleDisabledOptionsText(text: string) {
        return this.multipleDisabledOptionsText(text).click();
    }

    selectByContainsText(text: string) {
        this.optionByContainsText(text).click();
    }

    selectByText(text: string) {
        this.optionByText(text).click();
    }

    async waitToSelectMultipleDisabledOptionsText(text: string){
        var option = await this.multipleDisabledOptionsText(text)
        await browser.wait(ExpectedConditions.presenceOf(option[0]));
        await this.selectMultipleDisabledOptionsText(text)
    }

    async waitToSelectExactMatch(text: string) {
        var option = await this.optionByText(text);
        await browser.wait(ExpectedConditions.presenceOf(option));
        await option.click();
    }

    async waitToSelectByContainsText(text: string) {
        var option = await this.optionByContainsText(text);
        await browser.wait(ExpectedConditions.presenceOf(option));
        await option.click();
    }

    async waitToSelectByWhiteSpaceNormalized(text: string) {
        var option = await this.optionByWhiteSpaceNormalized(text);
        await browser.wait(ExpectedConditions.presenceOf(option));
        await option.click();
    }

    optionByValue(value: string) {
        return this.webelement.element(By.xpath("//option[@value='" + value + "']"))
    }
}