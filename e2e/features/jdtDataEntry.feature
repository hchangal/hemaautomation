Feature: JdtDataEntry
    As a user
    I want to enter jdt data

    @regression @smoke
    Scenario: user wants to enter jdt data
        Given User logs into application
        When User selects appropriate "project" and "jdt" and "reference" and "legaltxt"
        And Enter reference number and input data in legal field
        And Submit form
        Then jdt data gets updated