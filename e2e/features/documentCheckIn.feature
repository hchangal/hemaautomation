Feature: DocumentCheckIn
    As a user
    I want to check in document
    @regression @smoke @wip2 @failed @EdTest
    Scenario: user wants to check in loan document
        Given User logs into application
        When user on document checkin page User selects appropriate "TT8010106DC (Test Client)" and "HCTest001" and 'TestBatch001' and 'received' and "No File Type" and 'COPY' and 'ORIGINAL' and 'Generic Slip Sheet'
        Then user on document checkin page submit the loan
        Then user on document checkin page generate document