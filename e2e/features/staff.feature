Feature: Employee Resources
    As a user
    I want to access and change employee data 

    @regression @smoke!
    Scenario: SCI Staff photos enlarge when mouse moves over
        Given User logs into application
        When User selects appropriate "Employee Resources" and final "SCI Staff"
        And User hovers mouse over picture on SCI Staff page
        Then selected image enlarges