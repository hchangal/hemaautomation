Feature: ImageSearch
    As a user
    I want to verify image got uploaded

    @regression @wip4 @failed
    Scenario: user wants to search image already exist
        Given User logs into application
        When User on Image Search page selects appropriate "FinProd108" and "TT8010106DC (Test Client)" to test existing image
        And search for images
        And user on image search page selects appropriate "jdt" to view the JDT to test existing image
        Then Existing image displays under JDT

    @regression @wip4
    Scenario: user wants to search tif image already exist
        Given User logs into application
        When User on Image Search page selects appropriate "FinProd108" and "TT8010106DC (Test Client)" to test existing image
        And search for images
        And user on image search page selects appropriate "jdt" to view the JDT to test existing tif image
        Then Existing image displays under JDT

    @regression @smoke @wip4
    Scenario: user wants to merge pdf
        Given User logs into application
        When User on Image Search page selects appropriate "JohnTest001" and "TT8010106DC (Test Client)" to test existing image
        And search for images
        And user on image search page selects loan check box to merge and click merge
        Then Existing images gets merged

    @regression @smoke @wip4
    Scenario: user wants to merge tif
        Given User logs into application
        When User on Image Search page selects appropriate "JohnTest001" and "TT8010106DC (Test Client)" to test existing image
        And search for images
        And user on image search page selects loan check box to merge and click merge tif
        Then Existing images gets merged

    @regression @smoke @wip4
    Scenario: user wants to download image csv
        Given User logs into application
        When User on Image Search page selects appropriate "JohnTest001" and "TT8010106DC (Test Client)" to test existing image
        And search for images
        And user on image search page selects loan check box to merge and download csv
        Then Existing images gets merged


    @regression @smoke @wip11 @Test123
    Scenario: user wants to search image just uploaded
        Given User logs into application
        When User on Image Search page selects appropriate "JohnTest003" and "TT8010106DC (Test Client)" 
        And search for images
        And user on image search page selects appropriate "jdt" to view the JDT 
        Then uploaded image displays under JDT