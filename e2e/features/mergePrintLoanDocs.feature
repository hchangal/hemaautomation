Feature: MergePrintLoanDownload
    As a user
    I want to download merge document

    @regression @smoke
    Scenario: user wants to merge a loan
        Given User logs into application
        When User selects appropriate "project" and "merge Process"
        And Enter loan number and click mergeBtn
        Then pdf file gets downloaded

    @regression @failed
    Scenario: user wants to merge a loan an unsuccessfull attempt
        Given User logs into application
        When User selects appropriate "project" and "merge Process" and "invalid loan"
        And Enter loan number and click mergeBtn
        Then pdf file doesnt get downloaded