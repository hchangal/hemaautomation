Feature: DataReview
    As a user
    I want to test Data Review page

    @regression @smoke @EdTest
    Scenario: user wants to edit data review note
        Given User logs into application
        When User on DataReview Note page selects appropriate "TT8010106DC (Test Client)" and "2" and "Note" and "111 Irving street"
        And Make Few changes on note
        And Save changes on note
        Then data review gets updated on note
    @regression @smoke @EdTest
    Scenario: user wants to edit data review reference
        Given User logs into application
        When User on DataReview ReferenceData page selects appropriate "TT8010106DC (Test Client)" and "2" and "ReferenceData" and "legal description text"
        And Make Few changes on referencedata
        And Save changes on referencedata
        Then data review gets updated on referencedata