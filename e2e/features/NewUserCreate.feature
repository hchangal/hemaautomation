Feature: User Management
    As a administrative user
    I want to be able to create a new user
    So that user is able to login
    
    @regression 
    Scenario: Fail with short password and incorrect character types
        Given User logs into application
        And select Create New User button
        # When user on CreateNewUser page User enters appropriate "test17" and "test" and "lastNa17me" and "password"
        When user on CreateNewUser page User enters appropriate
        """
            {
                "username": "testProd11",
                "password": "a",
                "firstname": "test",
                "lastname":"17",
                "email":"test11Prod@bb.com"
            }
        """
        But password is one character not in username
        Then error popup occurs
        And error message "Password must be at least 8 characters long." is present
        And error message "Password must contain at least 2 types of characters (letters, numbers, or symbols)." is present
        And user is not able to login with new credentials

    @regression 
    Scenario: Fail with short password only
        Given User logs into application
        And select Create New User button
        # When user on CreateNewUser page User enters appropriate "userName" and "firstName" and "lastName" and "a" to test second scenario
        When user on CreateNewUser page User enters appropriate
        """
            {
                "username": "testProd12",
                "password": "a5",
                "firstname": "test",
                "lastname":"17",
                "email":"testProd12@bb.com"
            }
        """
        But password is one character and one number 
        Then error popup occurs
        And error message "Password must be at least 8 characters long." is present
        And user is not able to login with new credentials

    @regression 
    Scenario: Fail with incorrect character types
        Given User logs into application
        And select Create New User button
        # When user on CreateNewUser page User enters appropriate "userName" and "firstName" and "lastName" and "password" to test third scenario
        When user on CreateNewUser page User enters appropriate
        """
            {
                "username": "testProd13",
                "password": "password",
                "firstname": "test",
                "lastname":"17",
                "email":"testProd13@bb.com"
            }
        """
        But password is string only password not in username
        Then error popup occurs
        And error message "Password must contain at least 2 types of characters (letters, numbers, or symbols)." is present
        And user is not able to login with new credentials

    @regression  @failed
    Scenario: successfull user creation
        Given User logs into application
        And select Create New User button
        # When user on CreateNewUser page User enters appropriate "userName" and "firstName" and "lastName" and "password" to test third scenario
        When user on CreateNewUser page User enters appropriate
        """
            {
                "username": "ReleaseProdTest116B",
                "password": "P@ssword",
                "firstname": "irving",
                "lastname":"Test",
                "email":"ReleaseTestProd116B@irving.com"
            }
        """
        But password is following all the requirments
        Then error popup occurs
        And error message "User successfully created." is present
        
    # @wip
    # Scenario: Succeed with correct credentials
    #     Given on User Management Page
    #     And select Create New User button
    #     When user enters all required fields with unique username and email
    #     But password is over eight characters and contains one number
    #     Then user is able to login with new credentials
    #   @wip
    # Scenario: Create new user for SSO client
    #     Given on User Management Page with SSO client selected
    #     And select Create New User button
    #     When admin enters all required SSO user fields with unique username and email
    #     Then admin is back on User Management page with "User successfully created." status
    #     And user is able to login with new credentials

    #   @wip
    # Scenario: Try to create a user already created
    #     Given on User Management Page
    #     And select Create New User button
    #     When admin enters all required fields with credentials of existing user
    #     Then admin receives error message "Unable to create a user with this information.  Try a different username or email."
