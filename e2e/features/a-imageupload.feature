Feature: imageupload
    As an user
    I want to upload an image to a loan


    @regression @smoke @wip9 @failed @EdTest
    Scenario: user wants to upload an image
        Given User logs into application
        When on image search & upload page User selects appropriate "TT8010106DC (Test Client)" and "HCTest001"
        And Search form
        And goes to the loan
        And selects image submission category
        And selects an image and upload
        Then iamge gets uploaded

    # Due to the latency of upload, Use an existing project and loan/acct # that contains an already existing image.
    # Do not use project/acct# in Scenario: user wants to upload an image. It will not show up.
    # Default : TT8010106DC (Test Client) / FinProd108
    
    @regression @EdTest
    Scenario: user wants to upload an already existing image
        Given User logs into application
        When on image search & upload page User selects appropriate "TT8010106DC (Test Client)" and "FinProd108"
        And Search form
        And goes to the loan
        And selects image submission category for existing image test
        And selects an image and upload
        Then iamge gets warning
