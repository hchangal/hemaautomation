Feature: AsmtCountyOverdue
    As a user
    I want to pull AsmtCountyOverdue report

    @regression @smoke @EdTest
    Scenario: user wants to pull asmt html report
        Given User logs into application
        When User goes to reports for html 
        When User on asmt county page for html report and selects appropriate "TT8010106DC" and "Assignment of Mortgage"
        And click generate button for html report
        Then html report displays

    @regression @smoke @EdTest
    Scenario: user wants to pull asmt excel report
        Given User logs into application
        When User goes to reports for excel
        When User on asmt county page for excel report and selects appropriate "TT8010106DC" and "Assignment of Mortgage"
        And click generate button for excel report
        Then excel report displays

    @regression @smoke @EdTest
    Scenario: user wants to pull asmt csv report
        Given User logs into application
        When User goes to reports for csv
        When User on asmt county page for csv report and selects appropriate "TT8010106DC" and "Assignment of Mortgage"
        And click generate button for csv report
        Then csv report displays