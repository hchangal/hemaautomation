Feature: DocumentHistory
    As a user
    I want to check document history

    @regression @smoke @wip @failed
    Scenario: user wants to check document history
        Given User logs into application
        When user on document history User selects appropriate "project" and "reference" and "fileType" and "docType" and 'classType'
        Then user can view document history for that loan