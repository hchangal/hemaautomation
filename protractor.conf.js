// const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 120000,
  specs: [
    './e2e/**/MergePrintLoanDocs.spec.ts'
  ],
  capabilities: {
    
    browserName: 'chrome',
    'goog:chromeOptions': {
        'w3c': false
    }
},
  directConnect: true,
  baseUrl: 'https://www.dev.security-connect.com/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 90000,
    print: function() {}
  },
  beforeLaunch: function() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  onPrepare() {
    browser.ignoreSynchronization = true;
    browser.manage().window().maximize()
    // jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
}