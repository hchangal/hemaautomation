const jsonReports = process.cwd() + "/reports/json";

exports.config = {
  allScriptsTimeout: 30000,
  timeOutWait: 10000,
  
  specs: [
    './e2e/**/MergePrintLoanDocs.spec.ts'
  ],
  capabilities: {
 //   browserName: 'firefox'
    browserName: 'chrome',
    'goog:chromeOptions': {
      'w3c': false,
      args: ['--whitelisted-ips']
    }
  },

   seleniumAddress: "http://10.243.51.254:4444/wd/hub",


  directConnect: true,
  baseUrl: 'https://www.stg.security-connect.com/',
  params: {
    login: {
      username: process.env.testUser,
      password: process.env.testPassword
    }
  },
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),
  ignoreUncaughtExceptions: true,


  specs: [
    "e2e/features/*.feature",
  ],



  cucumberOpts: {
    compiler: "ts:ts-node/register",
    format: "json:./reports/cucumber_report.json",
    plugin:["pretty"],
    require: [
      'e2e/specs/stepDefs.ts',
      'e2e/support/hooks.ts',
  ],
    strict: true,
    'dry-run': false,
    tags: "@smoke",
  },

  
  beforeLaunch: function () {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  onPrepare() {
    browser.ignoreSynchronization = true;
    browser.manage().window().maximize()
    var createReports = require('./e2e/support/cucumber-reports')
    createReports()
  },
  onComplete: () => {
    var Reporter = require('./e2e/support/cucumber-html-report')
    Reporter()
  },

}